#!/usr/bin/env node

var express = require('express');
const low = require('lowdb');
const adapter = require('lowdb/adapters/FileSync');
const fs = require('fs');

var cors = require('cors');
var app = express();

var bodyParser = require('body-parser');

app.set("env", process.env.NODE_ENV || 'development');
app.disable('x-powered-by');
app.enable('trust proxy');
app.use(bodyParser.json()); // to support JSON-encoded bodies

// open db
const db = low(new adapter('storage.json'));
db.defaults({ queries: {}, fingerprints: {} }).write();

//  schema
/*

{
  queries: {
   $_query_id: [
      {
        "source": String, // source ip
        "method": String, // request method
        "content": {
          "host": String,
          "connection": String,
          "cache-control": String,
          "upgrade-insecure-requests": String,
          "user-agent": String,
          "accept": String,
          "dnt": String,
          "accept-encoding": String,
          "accept-language": String
        },
        "time": Date
      }
    ]
  },
  fingerprints: {
    $fingerprint: {
      content: {},
      accesses: integer
    }    
  }
}


*/

// CORS pre-flight
app.options('*', cors());

// middleware
app.use(function (req,res,next) {
  /*console.log({
    params: req.params,
    body: req.body,
    query: req.query,
    path: req.path
  });*/

  var id = req.query._q;

  let tree = db.get("queries");
  if (!tree.has(id).value()) { tree.set(id, []).write(); }

  data = {
    source: req.headers['x-real-ip'] || req.ip,
    path: req.path,
    method: req.method,
    content: req.headers,
    time: new Date(),
  }

  tree.get(id).push(data).write();

  next();
});


// no cache middleware
app.use(function (req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});


// statics
app.use(express.static("public"));

app.get('/fp', (req, res) => {
  res.header('Content-Type', 'application/javascript')
  let content = [];
  content.push(fs.readFileSync("./public/js/fingerprint2.js"));
  content.push(fs.readFileSync("./public/js/finger.js"));
  res.send(content.join("\n"));
});


app.post('/finger', cors(), (req, res) => {
  var fingerprint = req.body.result;
  let tree = db.get("fingerprints");

  if (!tree.has(fingerprint).value()) { tree.set(fingerprint, { components: req.body.components, count: 0 }).write(); }

  let visits = tree.get(fingerprint+".count").value();
  tree.set(fingerprint+".count", visits+1).write();

  res.send("OK");
});


// ----------------------
app.listen(3451);