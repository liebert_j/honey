## Honey honey honey!!!

What do you need:
---

  * nodejs
  * npm


Install dependencies:
---

```
npm install
```

Running the server
---

To start the server run: `node app.js`
It will start listeting on port 3451 by default.


How to
---

Simple fingerprint:

1. Add a file inside the `public` folder (image, js script, css stylesheet, etc.). It works like any other CDN.  
2. Link to your file from the website you want to track adding a custom generated ID as the query `_q`. e.g.: `<img src="http://yourhoney.dom/image.png?_q=made_up_id">`
3. Check storage.json getting filled with sweet sweet data
4. ???
5. Profits!


Advanced fingerprint:

1. Add the following link `<script src="http://yourhoney.dom/fp?_q=made_up_id">` to your site.
2. Check storage.json to see how it fills :D
3. ???
4. Moar profits!!


__CAVEAT__: it may not work with plugins like CanvasFingerprintBlock or NoScript. Simple fingerprint will work nonetheless. 
