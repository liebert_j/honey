
document.addEventListener("DOMContentLoaded", evt => {
  new Fingerprint2({ excludeWebGL: true }).get(function(result, components){
    let data = {
      result: result,
      components: components
    }

    let http = new XMLHttpRequest();
    http.open("POST","http://localhost:3451/finger", true);
    http.setRequestHeader("Content-type", "application/json");
    http.send(JSON.stringify(data));
  });
});
