#!/usr/bin/env node
var express = require('express');
const low = require('lowdb');
const adapter = require('lowdb/adapters/FileSync');
const fs = require('fs');
var path = require('path');

var app = express();

app.set("env", process.env.NODE_ENV || 'development');
app.disable('x-powered-by');
app.enable('trust proxy');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // to support JSON-encoded bodies

// open db
const db = low(new adapter('storage.json'));

// statics
app.use(express.static("public"));

// routes
app.get("/", function (req,res,next) {
  let data = db.read().value();
  res.render('index', { data: data });
});


// ----------------------
app.listen(3452);